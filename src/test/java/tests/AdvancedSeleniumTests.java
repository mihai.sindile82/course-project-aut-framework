package tests;

import Utils.OtherUtils;
import Utils.SeleniumUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.CookiePage;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import static Utils.SeleniumUtils.waitForGenericElement;

public class AdvancedSeleniumTests extends BaseTest{

    @Parameters({"searchKeyword"})
    @Test
    public void browserTest(String searchKeyword) throws InterruptedException {
        driver.get("https://google.com");
        test = extent.createTest("Browser Test");
        logInfoStatus("Search for element:" + searchKeyword);
        driver.switchTo().frame(0);
        waitForGenericElement(driver, By.id("introAgreeButton"),15).click();
        WebElement input = driver.findElement(By.name("q"));
        input.sendKeys(searchKeyword);
//      press ESC button from keybord
        Thread.sleep(2000);
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();
        logInfoStatus("Test End");
    }


    @Test
    public void cookieHomeworkTest(){
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(driver);
        System.out.println("Then we add our own cookie");
        Cookie cookie = new Cookie("HomeworkCookie", "easy peasy japanesey");
        driver.manage().addCookie(cookie);
        Assert.assertEquals(OtherUtils.getCoookieNumber(driver), 1);
        OtherUtils.printCookie(cookie);
        Assert.assertEquals(cookie.getName(), "HomeworkCookie");
        System.out.println("Checking name: OK");
        Assert.assertEquals(cookie.getValue(), "easy peasy japanesey");
        System.out.println("Checking value: OK");
        WebElement button = driver.findElement(By.id("delete-cookie"));
        button.click();
        System.out.println("This doesn't delete the cookie, it deletes the value from the gibberish cookie :)");
        OtherUtils.printCookies(driver);
        System.out.println("To delete the cookie we use magic: ");
        driver.manage().deleteCookieNamed(cookie.getName());
        OtherUtils.printCookies(driver);
        Assert.assertEquals(OtherUtils.getCoookieNumber(driver),0);
    }

    @Test
    public void homeworkGibberishTest(){
        CookiePage cp = new CookiePage(driver);
        cp.openCookiePage(hostname);
        cp.waitForCookiePage();
        System.out.println("Initial cookie state: ");
        OtherUtils.printCookies(driver);
        cp.setCookie();
        System.out.println("After we set the gibberish cookie");
        OtherUtils.printCookies(driver);
        System.out.println("Checking name");
        Cookie c = driver.manage().getCookieNamed("gibberish");
        Assert.assertEquals(c.getName(), "gibberish");
        System.out.println("OK");
        System.out.println("Checking value");
        Assert.assertFalse(c.getValue().isEmpty());
        System.out.println("OK");
        System.out.println("Deleting the value");
        cp.removeCookie();
        OtherUtils.printCookies(driver);
    }

    @Test
    public void cookieTest(){
        driver.get("https://google.com");
        OtherUtils.printCookies(driver);
        try {
            OtherUtils.printCookie(driver.manage().getCookieNamed("CONSENT1"));
        }
        catch (Exception e){
            System.out.println("Cookie not found !!!");
        }
        Cookie cookie = new Cookie("myCookie", "cookie123");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(driver);
        try {
            driver.manage().deleteCookieNamed("CONSENT");
            driver.manage().deleteCookieNamed("myCookie");
        }
        catch (Exception e){

        }
        OtherUtils.printCookies(driver);
    }

    @Test
    public void cookieCheckTest(){
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(driver);
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();
        OtherUtils.printCookies(driver);
    }

    @Test
    public void screenshotTest(){
        driver.get("https://google.com");
        OtherUtils.takeScreenShot(driver);
    }

    @Test
    public void alertTests(){
        driver.get(hostname + "/stubs/alert.html");
        WebElement alertButton = driver.findElement(By.id("alert-trigger"));
        WebElement confirmButton = driver.findElement(By.id("confirm-trigger"));
        WebElement promptButton = driver.findElement(By.id("prompt-trigger"));
        alertButton.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
        confirmButton.click();
        alert = driver.switchTo().alert();
        alert.dismiss();
        promptButton.click();
        alert = driver.switchTo().alert();
        alert.sendKeys("Text");
        alert.accept();
    }

    @Test
    public void hoverTest(){
        driver.get(hostname + "/stubs/hover.html");
        WebElement hoverButton = driver.findElement(By.xpath("/html/body/div/div/div[2]/div/button"));
        Actions actions = new Actions(driver);
//        actions = actions.moveToElement(hoverButton);
//        Action action = actions.build();
//        action.perform();
        actions.moveToElement(hoverButton).build().perform(); //inlantuire a celor 3 linii de mai sus
        WebElement itemMenu = driver.findElement(By.name("item 1"));
        System.out.println(itemMenu.getText());
        itemMenu.click();
    }

    @Test
    public void staleTest(){
        driver.get(hostname + "/stubs/stale.html");
        for (int i = 0; i <20; i++){
            WebElement staleButton = driver.findElement(By.id("stale-button"));
            staleButton.click();
        }
    }

    @Test
    public void clickStealerTest(){
        driver.get(hostname + "/stubs/thieves.html");
        WebElement button = driver.findElement(By.id("the_button"));
        button.click();
        Actions actions = new Actions(driver);
        //actions.sendKeys(Keys.ESCAPE).build().perform();
        WebElement closeModal = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[3]/button"));
        //This did not work for me
        //closeModal.click();
        //This did not work for me
        //actions.click(closeModal).build().perform();
        WebElement closeModalX = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[1]/button"));
        //same
        //actions.click(closeModalX).build().perform();
        WebElement modalText = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[2]/p"));
        System.out.println(modalText.getText());
        //works and doesn't :))
        actions.moveToElement(modalText).click(modalText).sendKeys(Keys.ESCAPE).build().perform();
        button.click();
    }

    @Test
    public void checkBoxTest(){
        driver.get(hostname + "/stubs/thieves.html");
        WebElement checkbox = driver.findElement(By.id("the_checkbox"));
        WebElement checkboxText = driver.findElement(By.xpath("//*[@id=\"div2\"]/label/span"));
        //Checkboxes sometimes need actions in order to be clicked
        //checkbox.click();
        Actions actions = new Actions(driver);
        actions.click(checkbox).build().perform();
        System.out.println(checkboxText.getText());
    }
}
