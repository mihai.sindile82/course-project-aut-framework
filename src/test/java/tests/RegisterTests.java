package tests;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.ProfilePage;
import pageObjects.RegisterPage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegisterTests extends BaseTest{

    @DataProvider(name = "submitPositiveDp")
    public Iterator<Object[]> registerPositiveDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // firstname, lastname, email, username, password,
        // confirmpassword (redundant, being positive, we will use password twice)
        dp.add(new String[]{"Mihai", "Sindile", "mihai@sindile.ro", "mihaisindile", "mihai123"});
        dp.add(new String[]{"Ion", "Agarbiceanu", "ion@ion.ro", "ionagarbiceanu", "ionel123"});
        dp.add(new String[]{"Dan", "Capitan", "dan@capitan.ro", "dancapitan", "dancapitandeplai123"});
        return dp.iterator();
    }

    @Test(dataProvider = "submitPositiveDp")
    public void registerPositive(String firstname, String lastname, String email, String username, String password) {
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.register(hostname,firstname, lastname, email, username, password, password);
        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Logged in user :" + profilePage.getUser());
        Assert.assertEquals(username, profilePage.getUser());

        profilePage.logOut();
    }

    @DataProvider(name= "submitNegativeDp")
    public Iterator<Object[]> registerDp(){
        Collection<Object[]> rdp = new ArrayList<Object[]>();
        rdp.add(new String[] {"", "", "", "", "", "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "", "", "", "", "",
                "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "Doe", "", "", "", "",
                "",
                "",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "", "", "",
                "",
                "",
                "",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "", "",
                "",
                "",
                "",
                "",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});
        rdp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "pass3333",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});


        return rdp.iterator();
    }

    @Test(dataProvider = "submitNegativeDp")
    public void negativeSubmitTest(String firstname, String lastname, String email, String username, String password,
                                   String confirmPassword, String firstNameErrMsg, String lastNameErrMsg, String emailErrMsg,
                                   String usernameErrMsg, String passErrMsg, String passConfirmErrMsg){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.register(hostname,firstname, lastname, email, username, password, confirmPassword);
        Assert.assertTrue(registerPage.checkErr(firstNameErrMsg, "firstNameErr"));
        Assert.assertTrue(registerPage.checkErr(lastNameErrMsg, "lastNameErr"));
        Assert.assertTrue(registerPage.checkErr(emailErrMsg, "emailErr"));
        Assert.assertTrue(registerPage.checkErr(usernameErrMsg, "usrErr"));
        Assert.assertTrue(registerPage.checkErr(passErrMsg, "passErr"));
        Assert.assertTrue(registerPage.checkErr(passConfirmErrMsg, "passConfirmErr"));
    }

}
