package pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

    private final WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "register-tab")
    WebElement registerTab;
    @FindBy(how = How.ID, using = "inputFirstName")
    WebElement firstNameInput;
    @FindBy(how = How.ID, using = "inputLastName")
    WebElement lastNameInput;
    @FindBy(how = How.ID, using = "inputEmail")
    WebElement emailInput;
    @FindBy(how = How.ID, using = "inputUsername")
    WebElement usernameInput;
    @FindBy(how = How.ID, using = "inputPassword")
    WebElement passwordInput;
    @FindBy(how = How.ID, using = "inputPassword2")
    WebElement passwordConfirmInput;
    @FindBy(how = How.ID, using = "register-submit")
    WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[3]/div/div[2]")
    WebElement firstNameErrMsg;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[4]/div/div[2]")
    WebElement lastNameErrMsg;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]")
    WebElement emailErrMsg;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[6]/div/div[2]")
    WebElement usernameErrMsg;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[7]/div/div[2]")
    WebElement passErrMsg;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[8]/div/div[2]")
    WebElement passConfirmErrMsg;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String hostname, String firstName, String lastName, String email, String username,
                         String password, String confirmPassword) {
        openRegisterPage(hostname);
        waitForRegisterTab();

        //we will take an extra measure of safety
        wait.until(ExpectedConditions.elementToBeClickable(firstNameInput));
        firstNameInput.click();
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.click();
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        emailInput.click();
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.click();
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.click();
        passwordInput.clear();
        passwordInput.sendKeys(password);
        passwordConfirmInput.click();
        passwordConfirmInput.clear();
        passwordConfirmInput.sendKeys(confirmPassword);

        submitButton.submit();
    }

    public void waitForRegisterTab() {
        driver.findElement(By.id("register-tab")).click();
        wait.until(ExpectedConditions.elementToBeClickable(registerTab));
    }

    public boolean checkErr(String error, String type) {
        if (type.equalsIgnoreCase("firstNameErr"))
            return error.equals(firstNameErrMsg.getText());
        else if (type.equalsIgnoreCase("lastNameErr"))
            return error.equals(lastNameErrMsg.getText());
        else if (type.equalsIgnoreCase("emailErr"))
            return error.equals(emailErrMsg.getText());
        else if (type.equalsIgnoreCase("usrErr"))
            return error.equals(usernameErrMsg.getText());
        else if (type.equalsIgnoreCase("passErr"))
            return error.equals(passErrMsg.getText());
        else if (type.equalsIgnoreCase("passConfirmErr"))
            return error.equals(passConfirmErrMsg.getText());
        return false;
    }

    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
        //#registration_panel in the url doesn't seem like best practice to me, but instead, clicking on register tab
        //sounds safer in case the anchor name is prone to change
        waitForRegisterTab();
        registerTab.click();
    }

}
