package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CookiePage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "delete-cookie")
    WebElement removeCookie;

    @FindBy(how = How.ID, using = "set-cookie")
    WebElement setCookie;

    public CookiePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void waitForCookiePage() {
        wait.until(ExpectedConditions.elementToBeClickable(removeCookie));
    }

    public void setCookie(){
        WebElement button = driver.findElement(By.id("set-cookie"));
        button.click();
    }

    public void removeCookie() {
        WebElement button = driver.findElement(By.id("delete-cookie"));
        button.click();
    }

    public void openCookiePage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/cookie.html");
        driver.get(hostname + "/stubs/cookie.html");
    }
}
